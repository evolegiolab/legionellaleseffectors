#! /bin/bash

#The purpose of this code is to blast the multiple alignment files for each gained effector against all eukaryotes
# Run where blast database is located

#Path to blast database
dbpath="/nobackup/blastdb/nr"

#for every concatenated and trimmed fasta file: 
for f in *concat_trimmed.faa; do

    echo "running blast on" "$f"
    outfile=$(echo "$f"  | sed 's/concat_trimmed.faa/blast_out/')
    cp $f ./blast/
    cd blast
    fdb=$(echo "$f" | sed 's/concat_trimmed.faa/blastdb/')
    #Make a new blast database out of fasta file
    makeblastdb -in $f -dbtype prot -out $fdb
    #Run blast against the file itself to create pssm file
    psiblast -in_msa $f -db $(echo -e "/seq/projects/adaptationRoads/effectors_TA/merged_effector_models/nodes_trees/blast/""$fdb") -ignore_msa_master -save_pssm_after_last_round -outfmt 6 -out $outfile -out_pssm $(echo -e "$outfile""_pssm")
    cd ..
done
    
