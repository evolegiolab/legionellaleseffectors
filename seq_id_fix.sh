#! /bin/bash

set -e

#this code changes the blast fasta sequence id so that the species name and accession are the only ones remaining

for f in *aligned_blast_hits.fasta; do # OR alternatively full_blast_hits.fasta
    newf=$(echo "$f" | sed 's/_hits.fasta/_hits_mod.fasta/g')
    > $(echo "$newf")
    #Getting each species only once
    s=$(cat "$f" |grep '^>' | awk -F"[" '{print $NF}' | sed 's/]//g' |awk '{if (!seen[$0]++) print}')
    
    echo "$f" 
    
    while read -r sp; do
	access=$(cat $f | grep "$sp" | awk 'NR==1{print $1}')
	species=$(echo "$sp" | sed 's/ /_/g')
	new_id=$(echo -e  "$access""_""$species")
	spe=$(echo "${sp}" | sed 's#\/#\\/#g')
	new_content=$(cat "$f" | sed -n '/'"${spe}"'/,/>/p'| sed '$d')

	match_count=$(echo "$new_content"  | grep '^>' | wc -l)
	echo "$match_count"
	if [ $match_count -gt 1 ]; then
	    new_content2=$(echo "$new_content" | sed -n '/'"${spe}"'/{p; :loop n; p; /'"${spe}"'/q; b loop}')
	    echo "$new_content2" | sed '1d;$d'|sed '$d'| sed  '1i'"$new_id" >> ${newf}
	else
	    echo "$new_content" | sed '1d'| sed '1i'"$new_id" >> ${newf}
	fi
    done<<<"$s"

done
