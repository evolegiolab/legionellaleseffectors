#! /bin/bash

#This script makes a table where the number of protein model copies per species per model are presented

set -e


#Sources
protGroups=$(cat ../ancestral_reconstruction/orthomcl/groups.txt)
orthoSeqs=$(cat ../ancestral_reconstruction/orthomcl/orthomcl.selection.tab)

if [ -e effector_occurrence_forAll.csv ]; then
    > effector_occurrence_forAll.csv
else
    touch effector_occurrence_forAll.csv
fi

if [ -e effector_occurrence_count.csv ]; then
    > effector_occurrence_count.csv
else
    touch effector_occurrence_count.csv
fi

models=$(grep -v ^RICGR models_to_merge.csv | awk '{print $3}' | awk -F',' '{print $1}'| sort | uniq)

#Building table
col=3
row=1
data=$(echo -e "row"",""sp"",")

while read -r models
do 
    data1=$(echo -e "$data""$models"",")
    data=$(echo "$data1") 
    #list all ids and prots under a model
    modelElems=$(echo "$protGroups"  | grep "$models" | awk -F' ' '{print $0}' RS=" " | sort)
    #counting duplicates
    idcount=$(echo "$modelElems" | awk -F'|' 'NR>1{print $1}' | sort | uniq -c)
    while read -r  elem ; do #for each id and protein in model do
	id=$(echo "$elem" | awk -F'|' '{gsub(/ /,"",$0); print $1}')
	sp=$(echo "$orthoSeqs" | grep -w "$id" | awk '{gsub(/ /,"",$0); print $1}'| awk -F'_' '{gsub(/ /,"",$0); print $0}')
	count=$(echo "$idcount" | grep -w "$id" | awk '{print $1}')
	#building table
	if [ "${#sp}" -gt 1 ]
	then
	    #if sp is already there
	    present=$(cat effector_occurrence_forAll.csv | grep -w "$sp" | wc -l)
	    present_count=$(cat effector_occurrence_count.csv | grep -w "$sp" | wc -l)
	    if [ $present -gt 0 ] && [ $present_count -gt 0 ]
	    then
		test=$(cat effector_occurrence_forAll.csv | awk -F ',' -v sp="$sp" -v col="$col" '{OFS = ","; if ($2 == sp) $col="1"} ; {print $0}')
		echo "$test" > effector_occurrence_forAll.csv
		withcount=$(cat effector_occurrence_count.csv | awk -F ',' -v sp="$sp" -v col="$col" -v count="$count" '{OFS = ","; if ($2 == sp) $col=count} ; {print $0}')
		echo "$withcount" > effector_occurrence_count.csv
		let row=row
	    else #Species not there yet
		commas=$(printf ',%.0s' $(seq 1 $(expr $col - 2)))
		test=$(echo -e "$row"",""$sp"",""$commas""1,")
		echo "$test" >> effector_occurrence_forAll.csv
		withcount=$(echo -e "$row"",""$sp"",""$commas""$count"",")
		echo "$withcount" >> effector_occurrence_count.csv
		let row=row+1
	    fi
	fi
    done < <(echo "$modelElems" | awk 'NR>1{print $0}')
    let col=col+1
done <<<"$models"

#Table headings
sed -i '1i\'"$data" effector_occurrence_forAll.csv
sed -i '1i\'"$data" effector_occurrence_count.csv
