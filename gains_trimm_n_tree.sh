#! /bin/bash

#with this code the 8 gained effectors from Legionellaceae, Coxiellacea, etc are trimmed and a tree is built

set -e
#source ~/.bash_profile

#getting models of nodes
nrs=(96 95 94 78 57)
nodes=$(echo "${nrs[@]}" | sed 's/ /\n/g')


echo -n "Do you want to trim aligned sequences? (yes/no):"

read answer1

if [ $answer1 = "yes" ]; then
    while read node ;do
	echo "getting models"
	line=$(cat effector_GainLoss.csv | grep ^"$node")
	echo "$line"
	#linecheck
	if [ $(echo "$line" | wc -l) -gt 1 ];then
	    echo "something went wrong, more than one node selected"
	else
	   gains=$(echo "$line" | awk -F'\t' '{print $4}' | sed 's/,/ /g') 
	fi
	if [ ${#gains[@]} -gt 0 ]; then
	    echo "running trimal"
	    for f in ${gains[@]}; do
	        file=$(echo -e "$f"".concat1_aligned.faa")
	        echo "trimming" "$file"
	        outname=$(echo "$file" | sed 's/concat1_aligned.faa/concat_trimmed.faa/')
	        #running trimal with max 20% gaps in column (gt=0.8)
	        trimal -in $file -out /seq/projects/adaptationRoads/effectors_TA/merged_effector_models/nodes_trees/$outname -htmlout /seq/projects/adaptationRoads/effectors_TA/merged_effector_models/nodes_trees/$outname.html -gt 0.8
	    done
	fi
    done<<<"$nodes"
fi

echo -n "Do you want to build a tree with iqtree? (yes/no):"

read answer2

if [ $answer2 = "yes" ]; then
    cd /seq/projects/adaptationRoads/effectors_TA/merged_effector_models/nodes_trees
    #running iqtree 
    for f in *concat_trimmed.faa; do
	echo "building tree from" "$f"
	iqtree -s $f -m LG+F+G -st AA -bb 1000 -nt AUTO -nm 5000 -redo
	echo "done"
    done
fi
echo -n "Do you want to build a trees with mixture models? (yes/no) (current recommendation, no):"

read answer3

if [ $answer3 = "yes" ]; then
    cd /seq/projects/adaptationRoads/effectors_TA/merged_effector_models/nodes_trees

    #running iqtree, this part of the program has been getting stuck each time without any errors
    for f in *.faa; do
	echo "building pmsf tree from" "$f"
	if [ -d pmfs_trees ]; then
	    if [ -e ./pmfs_trees/$f  ]; then
		cd ./pmfs_trees
	    else
		cp $f ./pmfs_trees
	        cp $f.treefile ./pmfs_trees

		cd ./pmfs_trees
	    fi
        else
	    mkdir pmfs_trees
	    cp ${f} ./pmfs_trees
	    cp ${f}.treefile ./pmfs_trees
	    cd ./pmfs_trees
	fi
	treef=$(echo -e "$f"".treefile")
	echo "$file" "sitefrequencies"
	iqtree -s ${f} -m LG+C20+F+G -st AA -ft ${treef} -n 0
	#fi
	#cd ..
	echo "done"
	sitef=$(echo -e "$f"".sitefreq")
	echo "$file" "tree"
	iqtree -s ${f} -m LG+C20+F+G -fs ${sitef} -bb 1000 -nt AUTO 
    done
fi
