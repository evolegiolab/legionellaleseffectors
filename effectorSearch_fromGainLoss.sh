#! /bin/bash

set -e

#Purpose of this scipt is to go through loss and gain tabs in nodes_GainLoss and check for effector models that we know from before
modellist="/seq/projects/adaptationRoads/effectors_TA/models_to_merge.csv"

models=$(grep -v ^RICGR "$modellist" | awk '{print $3}' | awk -F',' '{print $1}'| sort | uniq)

#list nodes (and assign to annotation?)
#for specific nodes
#nodes=$(grep "node" gains_n_losses_pbtree.txt| awk '{print $2}')
#for all nodes
nodes=$(echo {1..110} | sed 's/ /\n/g')
#echo "$nodes"
#96 Legionellales,94 Coxiellaceae,92 Aquicella, 76 Rickettsiella, 73 Coxiella, 56 Legionellaceae 53 Legionella pneumophila-group
#get corresponding gains and losses files
> effector_GainLoss.csv
while read node; do
    nodegains=""
    nodelosses=""
    loctagsgain=""
    loctagsloss=""
    gains=$(cat ./nodes_GainLoss/${node}_gain.tab)
    losses=$(cat ./nodes_GainLoss/${node}_loss.tab)
    #loop through effectors from list
    while read model ; do
	#grep gains and losses files for effector models
	gainpres=$(cat ./nodes_GainLoss/${node}_gain.tab | grep -w "$model" | wc -l)
	losspres=$(cat ./nodes_GainLoss/${node}_loss.tab | grep -w "$model" | wc -l)
	if [ $gainpres -gt 0 ]; then
	    nodegains=$(echo -e "$nodegains""$model"",")
	    #getting locTags for gained models
	    gloctag=$(grep -v ^RICGR "$modellist" |awk '{$3=substr($3,1,12);print $1,$3}'| grep "$model" | awk '{print $1}')
	    #echo "gained lt""$gloctag"
	    glpgs=$(echo "$gloctag"| grep 'lpg' | xargs)
	    gcbus=$(echo "$gloctag"| grep 'CBU_' | xargs)
	    gllos=$(echo "$gloctag"| grep 'LLO_' | xargs)
	    if [ $(echo "$gloctag" | sed '/^\s*$/d' | wc -l) -gt 0 ]; then
		if [ $(echo "$glpgs"|sed '/^\s*$/d'| wc -l ) -gt 0 ]; then
		    glpg2=$(echo "$glpgs" | sed 's/ /,/g')
		    #echo "glpg2:""$glpg2"
		    loctagsgain=$(echo -e "$loctagsgain""$glpg2"",")
		elif [ $(echo "$gcbus" |sed '/^\s*$/d'| wc -l) -gt 0 ]; then
		    loctagsgain=$(echo -e "$loctagsgain""$gcbus"",")
		else
		    loctagsgain=$(echo -e "$loctagsgain""$gllos"",")
		fi 
	    fi
	fi
	if [ $losspres -gt 0 ]; then
	    nodelosses=$(echo -e "$nodelosses""$model"",")
	    #getting locTags for lost models
	    lloctag=$(grep -v ^RICGR "$modellist" |awk '{$3=substr($3,1,12);print $1,$3}'| grep "$model" | awk '{print $1}')
	    #echo "lost lt" "$lloctag"
	    llpgs=$(echo "$lloctag" | grep 'lpg'| xargs)
	    lcbus=$(echo "$lloctag" | grep 'CBU_' | xargs)
	    lllos=$(echo "$lloctag"| grep 'LLO_'  | xargs)
	    if [ $(echo "$lloctag" | sed '/^\s*$/d' | wc -l) -gt 0 ]; then
		if [ $(echo "$llpgs" |sed '/^\s*$/d'| wc -l) -gt 0 ]; then
		    llpg2=$(echo "$llpgs" | sed 's/ /,/g')
		    #echo "llpg2:""$llpg2"
		    loctagsloss=$(echo -e "$loctagsloss""$llpg2"",")
		elif [ $(echo "$lcbus" |sed '/^\s*$/d'| wc -l ) -gt 0 ]; then
		    loctagsloss=$(echo -e "$loctagsloss""$lcbus"",")
		else
		    loctagsloss=$(echo -e "$loctagsloss""$lllos"",")
		fi
	    fi
	fi

    done < <(echo "$models" | awk 'NR>0{print $0}') 
    gaincount=$(echo "$nodegains" | awk -F',' '{print (NF-1)}')
    losscount=$(echo "$nodelosses" | awk -F',' '{print (NF-1)}')
    ltgaincount=$(echo "$loctagsgain" | awk -F',' '{print (NF-1)}')
    ltlosscount=$(echo "$loctagsloss" | awk -F',' '{print (NF-1)}')
    if [ $gaincount -eq -1 ]; then
	gaincount=0
    fi
    if [ $losscount -eq -1 ]; then
	losscount=0
    fi
     if [ $ltgaincount -eq -1 ]; then
	ltgaincount=0
    fi
    if [ $ltlosscount -eq -1 ]; then
	ltlosscount=0
    fi
    echo -e "$node""\t""$gaincount""\t""$losscount""\t""$nodegains""\t""$nodelosses""\t""$ltgaincount""\t""$ltlosscount""\t""$loctagsgain""\t""$loctagsloss" >> effector_GainLoss.csv
done<<<"$nodes"

#table modification
sed -i '1i\node\tnrgains\tnrlosses\tgains\tlosses\tnrlocTagsGained\tnrlocTagsLost\tlocTagsGained\tlocTagsLost' effector_GainLoss.csv
sed -i 's/start,//g;s/start//g;s/,$//g'  effector_GainLoss.csv 




