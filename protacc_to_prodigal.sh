#!/bin/bash
# This script goes through each protein annotation number in combined_eff_info.csv and finds the corresponding ID given by prodigal, in the reannotations- files. Then the orthomcl protein families are searched for this ID. The results are printed in effs_in_protFamilies.csv

#01/02/2018 TA

set -e

#paths and reading of prodigal annotation files
prodAnnotLeg=$(cat ../genomic_sources/reannotations/Legionella_pneumophila_subsp_pneumophil_1.tbl | awk -F'\t' 'NR>1{gsub(/ /,"",$0);print $1,FS,$8}')
prodAnnotCox=$(cat ../genomic_sources/reannotations/Coxiella_burnetii_RSA_493.tbl | awk -F'\t' 'NR>1{gsub(/ /,"",$0);print $1,FS,$8}')
prodAnnotRick=$(cat ../genomic_sources/reannotations/Rickettsiella_grylli.tbl | awk -F'\t' 'NR>1{gsub(/ /,"",$0);print $1,FS,$8}')
prodAnnotLong=$(cat ../genomic_sources/reannotations/Legionella_longbeachae_NSW150.tbl | awk -F'\t' 'NR>1{gsub(/ /,"",$0);print $1,FS,$8}')

#paths and reading effector information and orthomcl grouping
protAcc=$(cat ./combined_eff_info.csv | awk -F'\t' 'NR>1{gsub(/ /,"",$0);print $1,FS,$3,FS,$(NF-2),FS,$7,FS,$14}')
protGroups=$(cat ../ancestral_reconstruction/orthomcl/groups.txt)

>effs_in_protFamilies.csv

while IFS=$'\t' read -r  locTag sp acc dotIcm comm ; do
    #checking if prot accession exists (is not empty) and getting the prodigal annotation
    if [ ${#acc} != 0 ]
    then
	#getting prodigal annotation by species
	if [ $sp = "legionella" ]
	then
	    elem=$(echo "$prodAnnotLeg" | awk -F'\t' '{gsub(/ /,"",$0);print $0}'| grep $(echo "$acc" | xargs) | awk -F'\t' '{print $1}')
	elif [ $sp = "coxiella" ]
	then 
	    elem=$(echo "$prodAnnotCox" | awk -F'\t' '{print $0}'| grep $(echo "$acc"|xargs) | awk -F'\t' '{gsub(/ /,"",$0);print $1}')
	elif [ $sp = "rickettsiella" ]
	then
	    elem=$(echo "$prodAnnotRick" | awk -F'\t' '{print $0}'| grep $(echo "$acc"|xargs) | awk -F'\t' '{gsub(/ /,"",$0);print $1}')
	elif [ $sp = "longbeachae" ]
	then
	    elem=$(echo "$prodAnnotLong" | awk -F'\t' '{print $0}'| grep $(echo "$acc"|xargs) | awk -F'\t' '{gsub(/ /,"",$0);print $1}')
	fi
    fi
    #searching for elem/prodigal annotation in groups.txt
    if [ ${#elem} != 0 ] 
    then
	models=$(echo "$protGroups" | awk -F' ' '{print $0}'| grep -w "$elem"|sed 's/://g'| awk '{print $1}')
	count=$(echo "$models"| grep -cvx ' ')
	echo -e "$locTag"'\t'"$sp"'\t'"$acc"'\t'"$dotIcm"'\t'"$comm"|awk  -F'\t' -v v1="$elem" -v v2="$models" '{gsub(/ /,"",$0);print $0,FS,v1,FS,v2}'>>effs_in_protFamilies.csv
    fi
done <<<"$protAcc"

#Adding table heading
sed -i 1i"locTag\tsp\tprotein\tdotIcm\tcomments\tprodigalId\tmodel" effs_in_protFamilies.csv
