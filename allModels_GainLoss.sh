#! /bin/bash

set -e

#Purpose of this scipt is to go through loss and gain tabs in nodes_GainLoss

#list nodes (and assign to annotation?)
#for all nodes
nodes=$(echo {1..110} | sed 's/ /\n/g')
#96 Legionellales,94 Coxiellaceae,92 Aquicella, 76 Rickettsiella, 73 Coxiella, 56 Legionellaceae 53 Legionella pneumophila-group
#get corresponding gains and losses files
> allModels_GainLoss.csv
while read node; do
    gainpres=""
    losspres=""
    gainlist=""
    losslist=""

    gains=$(cat ./nodes_GainLoss/${node}_gain.tab | awk 'NR>2{print $2}')
    losses=$(cat ./nodes_GainLoss/${node}_loss.tab | awk 'NR>2{print $2}')
    gainpres=$(echo "$gains" | grep "model" | wc -l)
    if [ $gainpres -gt 0 ]; then
        gainlist=$(echo "$gains" | awk 'BEGIN{ORS=","}{print}')
    fi
    losspres=$(echo "$losses" | grep "model" | wc -l)
    if [ $losspres -gt 0 ]; then
        losslist=$(echo "$losses" | awk 'BEGIN{ORS=","}{print}')
    fi
    
    if [ $gainpres -eq -1 ]; then
	gainpres=0
    fi
    if [ $losspres -eq -1 ]; then
	losspres=0
    fi
    echo -e "$node""\t""$gainpres""\t""$losspres""\t""$gainlist""\t""$losslist" >> allModels_GainLoss.csv
done<<<"$nodes"

#table modification
sed -i '1i\node\tnrgains\tnrlosses\tgains\tlosses' allModels_GainLoss.csv
sed -i 's/start,//g;s/start//g;s/,$//g'  allModels_GainLoss.csv 
