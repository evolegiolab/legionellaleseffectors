#! /bin/bash
#This script trims aligned sequences with Trimal and builds a phylogenetic tree with iqtree using the WAG model

set -e
source ~/.bash_profile

if [ -d trimms_n_trees ]
then :
else
    mkdir trimms_n_trees
fi

echo -n "Do you want to trim aligned sequences? (yes/no):"

read answer1

if [ $answer1 = "yes" ]; then
 
    cd merged_families

    for file in *.faa; do
	echo "trimming" "$file"
	outname=$(echo "$file" | sed 's/aligned.faa/trimmed/')
	#running trimal with max 20% gaps in column (gt=0.8)
	trimal -in $file -out /seq/projects/adaptationRoads/effectors_TA/trimms_n_trees/$outname.faa -htmlout /seq/projects/adaptationRoads/effectors_TA/trimms_n_trees/$outname.html -gt 0.7
    done
fi

echo -n "Do you want to build a tree with iqtree? (yes/no):"

read answer2

if [ $answer2 = "yes" ]; then
    cd /seq/projects/adaptationRoads/effectors_TA/trimms_n_trees

    #running iqtree 
    for f in *.faa; do
	echo "building tree from" "$f"
	iqtree -s $f -m WAG+F+G -st AA -keep-ident -bb 10000 -nt AUTO -nm 5000 -redo
	echo "done"
    done
fi
echo -n "Do you want to build a trees with mixture models? (yes/no):"

read answer3

if [ $answer3 = "yes" ]; then
    cd /seq/projects/adaptationRoads/effectors_TA/trimms_n_trees

    #running iqtree 
    for f in *.faa; do
	treef=$(echo -e "$f"".treefile")
	echo "building pmsf tree from" "$f"
	cd ./pmfs_trees
	f=$(echo -e "/seq/projects/adaptationRoads/effectors_TA/trimms_n_trees/""$f")
	echo "$f"
	iqtree -s $f -m LG+C20+F+G -st AA -ft $treef -keep-ident -nm 5000 -b 100 
	
	cd ..
	echo "done"
    done
fi


