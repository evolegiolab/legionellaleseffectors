#!/bin/bash

#This script will double check the homolog-status of effectors. It will first go through each effector that has homologs in other species,list all the locTags and concatenate corresponding fasta files. Then the concatenated fasta will be run through mafft align. 

set -e

#Sources
effectors=$(cat combined_eff_info.csv | tr -d ' ' |awk -F'\t' 'NR>1{print $1,FS,$3,FS,$5,FS$7,FS,$9,FS,$10,FS,$11,FS,$13,FS,$14,FS,$15,FS,$NF}')
modelEffs=$(cat effs_in_protFamilies.csv | grep "model" | tr -d ' ' | awk -F'\t' 'NR>1{print $0}')


while IFS=$'\t' read -r locTag sp inLEOG dotIcm cox llo ldg rick comm prot file <&8
do
    modCheck=$(echo "$modelEffs"  | awk -F'\t' '{print $0}'| grep $prot |xargs | awk -F'\t' '{gsub(/ /,"",$0);print $0}')
    #check to take only those effectors that do have a match in protein families
    if [ ${#modCheck} != 0 ]
    then 
	#getting a list of all orthologs/homologs
	homols=($(echo -e "$locTag" "$cox" "$llo" "$ldg" "$rick" |awk -F' ' '{gsub(","," ",$0); print $0}'))
	#getting and concatenatig fasta-files corresponding to homols
	if [ ${#homols[@]} -gt 1 ]
	then
	    for locus in ${homols[@]}; do
		loc=$(echo "$locus" | xargs)
		loctag=$(echo "$locTag" |xargs)
		if [ -e ./legEffectors/$loc.fasta ]
		then
		    if [ -e ./concat_homologs/concat_$loctag.fasta ]
		    then :
		    else
			cat ./legEffectors/$loc.fasta >> ./concat_homologs/concat_$loctag.fasta
		    fi
		fi
	    done
	fi
    fi
done 8< <(echo "$effectors" | awk -F'\t' '{print $0}')

#running mafft align for all homologs
cd concat_homologs
for file  in *.fasta
do
    mafft --auto --amino --clustalout --distout --quiet $file   >  ${file%fasta}_aligned
    echo "aligned $file"
done

