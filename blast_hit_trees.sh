#! /bin/bash

#The purpose of this script is to run iqtree with LG model on the blast hits combined with the model sequences

set -e
source ~/.bash_profile

#realigning blast seqs to already aligned model seqs


echo -n "Do you want to realign blast and model sequences and trimm the alignment? (yes/no):"

read answer1

if [ $answer1 == "yes" ]; then 
    echo "Running mafft and trimming"
    models=('model0110918' 'model0111073' 'model0111084' 'model0111097' 'model0111236' 'model0112144' 'model0112917' 'model0113029')
    
    for model in ${models[@]}; do
	if [ -e ${model}.concat1_aligned.faa ]; then
            echo "Aligned model file present."
	else
	    cd ..

	    for f in ${model}.concat1_aligned.faa; do 
		cp $f ./gainedEffector_blast_hit_trees
	    done
	    cd ./gainedEffector_blast_hit_trees
	fi
	blastfile=$(echo -e "$model""_aligned_blast_hits_mod.fasta") #OR full_blast_hits_mod.fasta
	modelfile=$(echo -e "$model"".concat1_aligned.faa")
	output=$(echo -e "$model""_w_aligned_comb_aligned.faa")
	if [ -e "$blastfile" ]; then
	    newcombo=$(cat "$blastfile" "$modelfile" > ${model}_w_aligned_newcombo.fasta)
	    newalign=$(echo -e "$model""w_aligned_newcombo_aligned.faa")
	    echo "mafft on" "$model"
	    mafft --add ${blastfile} --keeplength ${modelfile} > ${output}
	    mafft --localpair --amino --maxiterate 1000 ${model}_w_aligned_newcombo.fasta > $newalign
	#Trimal
	    trimout=$(echo "$output" | sed 's/.faa/_trimmed.faa/')
	    newtrimout=$(echo "$newalign" | sed 's/.faa/_trimmed.faa/')
	    echo "trimal on" "$model"
	    trimal -in ${output} -out ${trimout} -gt 0.8   
	    trimal -in ${newalign} -out ${newtrimout} -gt 0.8 
	fi
    done
fi
echo -n  "Do you want to build iqtrees with LG? (yes/no):"

read answer2

if [ $answer2 == "yes" ]; then
    for f in *aligned*aligned_trimmed.faa; do
	echo "building tree from" "$f"
	iqtree -s $f -m LG+F+G -st AA -bb 1000 -nt AUTO -redo
	echo "done"
    done
fi

