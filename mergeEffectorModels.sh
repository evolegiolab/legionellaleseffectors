#! /bin/bash
#This script gets all sequences per model, merges sequences and aligns them with MAFFT  

set -e

#Sources
protGroups="/seq/projects/adaptationRoads/ancestral_reconstruction/orthomcl/groups.txt"
orthoSeqs="/seq/projects/adaptationRoads/ancestral_reconstruction/orthomcl/orthomcl.selection.tab"
modelList=$(cat ./models_to_merge.csv)

models=$(grep -v ^RICGR ./models_to_merge.csv | awk '{print $3}' | awk -F',' '{print $1}'| sort | uniq)

echo -n "Do you want to concatenate sequences from effector models? (yes/no):"
read answer1

if [ $answer1 = "yes"  ]; then
    echo "Concatenating models with effector proteins..."

    if [ -d ./merged_effector_models ]
    then :
    else
	mkdir merged_effector_models
    fi
    while IFS=  read -r model
    do 
	> ./merged_effector_models/"${model}".concat.fasta
	modelElems=$(cat "$protGroups" | grep "$model"  | awk -F' ' '{print $0}' RS=" ")
	while read -r  elem ; do #for each id and protein in model do
	    id=$(echo "$elem" | awk -F'|' '{gsub(/ /,"",$0); print $1}')
	    prot=$(echo "$elem" | awk -F'|' '{gsub(/ /,"",$0); print $2}')
	    seqPath=$(cat "$orthoSeqs" | grep -w "$id" | awk '{gsub(/ /,"",$0); print $NF}')
	    sp=$(cat "$orthoSeqs" |grep -w "$id" | awk '{gsub(/ /,"",$0); print $1}') 
	    end_check=$(grep '*$' "$seqPath" | wc -l)
	    if [ $end_check -gt 0 ]; then
		protSeq=$(cat "$seqPath" | sed -n "/""$prot""\b/,/*/p")
		trimProt=$(echo "$protSeq" |sed 's/*//;s/>//'| awk -v sp="$sp" -v var="$prot" -v mod="$model2" '{if($1 && NR==1) $1=">"sp"_"var"_"mod; print $1}')
            else
		protSeq=$(cat "$seqPath" |sed -n "/""$prot""\b/,/^>/p"| sed '$ d')
		trimProt=$(echo "$protSeq" | sed 's/*//;s/>//'| awk -v sp="$sp" -v var="$prot" -v mod="$model2" '{if($1 && NR==1) $1=">"sp"_"var"_"mod; print $1}')
	    fi

	    echo "$trimProt" | sed '/^\s*$/d' >> ./merged_effector_models/"${model}".concat.fasta
	done < <(echo "$modelElems" | awk 'NR>1{print $0}')
    done <<<"$models"
fi


echo -n  "Do you want to align sequences with MAFFT? (yes/no):"

read answer2

if [ $answer2 = "yes"  ]; then

    echo "Aligning sequences with MAFFT..."
    #aligning sequences 
    cd /seq/projects/adaptationRoads/effectors_TA/merged_effector_models

    for file in *.fasta ; do
	f=$(echo "$file" | awk '{gsub(".fasta",//,$0); print $0}')
	#mafft linsi run
	mafft --localpair --amino --maxiterate 1000 --anysymbol --quiet $file > ${f}_aligned.faa
	echo "aligned $file"
    done
    echo "...done"
fi
echo "End of script"
